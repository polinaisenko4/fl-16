export const root = document.getElementById('root'),
  ref = {
  addTweetBtn: document.querySelector('.addTweet'),
  tweetContainer: document.getElementById('list'),
  mainContainer: document.getElementById('tweetItems'),
  tweetsArray: JSON.parse(localStorage.getItem('tweets')),
  alertMessage: document.getElementById('alertMessageText'),
  alertContainer: document.getElementById('alertMessage'),
  timeOut: 2000
};
location.hash = '';