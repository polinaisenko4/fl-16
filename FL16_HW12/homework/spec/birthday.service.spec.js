// Your code goes here
const BirthdayService = require('../src/birthday.service.js');

const NOW_YEAR = 2021;
const POSSIBLE_YEAR = 2000;
const POSSIBLE_MOUNTH = 8;
const POSSIBLE_MOUNTH_2 = 2;
const TICK = 200;

describe('Birthday service', () => {
    beforeEach(() => {
        jasmine.clock().install();
        const baseDate = new Date(NOW_YEAR, 0, 1, 0, 0, 0);
        jasmine.clock().mockDate(baseDate);
    });
    it('should show when birthdays day is today', function (done) {
        BirthdayService.howLongToMyBirthday(new Date(POSSIBLE_YEAR, 0, 1, 0, 0, 0))
            .then(result => {
                expect(result).toEqual('Hooray!!! It is today!')
                done()
            });
        jasmine.clock().tick(TICK);
    })
    it('should fail when aruments are not a Date', function (done) {
        BirthdayService.howLongToMyBirthday(true)
            .catch((err) => {
                expect(err.message).toEqual('Wrong argument!');
                done()
            });
        jasmine.clock().tick(TICK);
    })
    it('should tell that you already celebrated', (done) => {
        BirthdayService.howLongToMyBirthday(new Date(POSSIBLE_YEAR, POSSIBLE_MOUNTH, 1, 0, 0, 0)) 
            .then(result => {
                expect(result).toEqual(`Oh, you have celebrated it 242 day/s ago, don't you remember?`)
                done()
            });
        jasmine.clock().tick(TICK);
    })
    it('should tell that your birthday soon', (done) => {
        BirthdayService.howLongToMyBirthday(new Date(POSSIBLE_YEAR, POSSIBLE_MOUNTH_2, 1, 0, 0, 0)) 
            .then(result => {
                expect(result).toEqual(`Soon...Please, wait just 58 day/days`)
                done()
            });
        jasmine.clock().tick(TICK);
    })
    afterEach(() => {
        jasmine.clock().uninstall();
    })
});