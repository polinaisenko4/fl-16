
const getBigestNumber = require('../src/get-bigest-number');

describe('getBigestNumber function', () => {
  it('should return "if at least one argument’s type is not a number"', () => {
    try {
      getBigestNumber(1, '2')
    } catch (err) {
      expect(err.message).toEqual('All argumensts must be a number!');
    }
  });

  it('should return "if function has less argument’s then 2"', () => {
    try {
      getBigestNumber(1)
    } catch (err) {
      expect(err.message).toEqual('Please, enter more arguments');
    }
  });

  it('should return "if function has more argument’s then 10"', () => {
    try {
      getBigestNumber(1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1)
    } catch(err) {
      expect(err.message).toEqual('Please, enter less arguments');
    }
  });

  it('should return biggest number from arguments', () => {
    expect(getBigestNumber(0, 1, 1, 1)).toEqual(1);
  });

});