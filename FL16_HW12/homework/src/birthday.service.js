const MS_PER_DAY = 1000 * 3600 * 24;
const HALF_YEAR = 6;
const MINUS_DAY = -1;
const DELAY = 100;

const BirthdayService = {
    howLongToMyBirthday(date) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (!(date instanceof Date)) {
                    return reject(new Error('Wrong argument!'));
                }
                const today = new Date();
                const birthdayDateInCurrentYear = new Date(today.getFullYear(), date.getMonth(), date.getDate());
                const daysBetweenDates = Math.floor(Math.abs(
                    (today.getTime() - birthdayDateInCurrentYear.getTime()) / MS_PER_DAY
                ));
                if (
                    today.getMonth() === date.getMonth()
                    && today.getDate() === date.getDate()
                ) {
                    return resolve(this.congratulateWithBirthday());
                } else if (
                    date.getMonth() > today.getMonth()
                    && date.getMonth() - today.getMonth() > HALF_YEAR + (today.getDay() > date.getDay() ? 0 : MINUS_DAY)
                    || today.getMonth() > date.getMonth()
                    && today.getMonth() - date.getMonth() < HALF_YEAR + (today.getDay() > date.getDay() ? 0 : MINUS_DAY)
                ) {
                    return resolve(this.notifyPastTime(daysBetweenDates));
                } else {
                    return resolve(this.notifyWaitingTime(daysBetweenDates));
                }
            }, DELAY);
        });
    },

    notifyPastTime(days) {
        return `Oh, you have celebrated it ${days} day/s ago, don't you remember?`
    },

    notifyWaitingTime(days) {
        return `Soon...Please, wait just ${days} day/days`
    },

    congratulateWithBirthday() {
        return `Hooray!!! It is today!`;
    }


}

// BirthdayService.howLongToMyBirthday(new Date(2021, 6, 23)).then(console.log); 

module.exports = BirthdayService;