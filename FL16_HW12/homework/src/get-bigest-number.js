const LEAST_NUM = 2;
const BIGGEST_NUM = 10;

const getBigestNumber = (...args) => {
    for(let i in args) {
        if (typeof args[i] !== 'number') {
            throw new Error('All argumensts must be a number!')
        }
    }

    let numArr = [...args];

    if (numArr.length < LEAST_NUM) {
        throw new Error('Please, enter more arguments')
    } else if (numArr.length > BIGGEST_NUM) {
        throw new Error('Please, enter less arguments')
    }

    return Math.max.apply(Math, numArr);
};


module.exports = getBigestNumber;