'use strict';

const CONSTRUCTOR_ARG_NUM = 2;

class PizzaException extends Error {
    constructor(message) {
        super(message);
        this.log = message;
    }
}

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */

class Pizza {
    constructor (size, type) {
        if (arguments.length !== CONSTRUCTOR_ARG_NUM) {
            throw new PizzaException(`Required two arguments, given: ${arguments.length}`)
        }
        if (!Pizza.allowedSizes.find(s => s === size.id)) {
            throw new PizzaException(`Invalid size`);
        }
        if (!Pizza.allowedTypes.find(t => t === type.id)) {
            throw new PizzaException(`Invalid type`);
        }
        this.size = size;
        this.type = type;
        this.extra = [];
    }

    getSize() {
        return this.size.name;
    }
    
    getPrice() {
        const price = this.size.price + this.type.price; 
        return this.extra.reduce((p, extra) => {
            return p + extra.price;
        }, price);
    }

    addExtraIngredient(ingredient) {
        if (arguments.length !== 1) {
            throw new PizzaException(`Required one argument, given: ${arguments.length}`);
        }
        if (!Pizza.allowedExtraIngredients.find(i => i === ingredient.id)) {
            throw new PizzaException(`Invalid ingredient`);
        }
        if (this.extra.find(i => i.id === ingredient.id)) {
            throw new PizzaException(`Duplicate ingredient`);
        }
        this.extra.push(ingredient);
    }

    removeExtraIngredient(ingredient) {
        if (arguments.length !== 1) {
            throw new PizzaException(`Required one argument, given: ${arguments.length}`);
        }
        if (!Pizza.allowedExtraIngredients.find(i => i === ingredient.id)) {
            throw new PizzaException(`Invalid ingredient`);
        }
        if (!this.extra.find(i => i.id === ingredient.id)) {
            throw new PizzaException(`Ingredient not found`);
        }
        this.extra = this.extra.filter(i => i.id !== ingredient.id);
    }

    getPizzaInfo() {
        const extraNames = this.extra.reduce((str, extra, index) => {
            if (index < this.extra.length - 1) {
                return str + extra.name + ',';
            } else {
                return str + extra.name;
            }
        }, '') || 'None';
        return `Size: ${this.size.name}, type: ${this.type.name}; ` +
        `extra ingredients: ${extraNames}; prize: ${this.getPrice()}UAH`;
    }

    getExtraIngredients() {
        return this.extra.map(e => e.name);
    }
}

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = {name: 'SMALL', price: 50, id: 'SIZE_S'};
Pizza.SIZE_M = {name: 'MIDDLE', price: 75, id: 'SIZE_M'};
Pizza.SIZE_L = {name: 'LARGE', price: 100, id: 'SIZE_L'};

Pizza.TYPE_VEGGIE = {name: 'VEGGIE', price: 50, id: 'TYPE_VEGGIE'};
Pizza.TYPE_MARGHERITA = {name: 'MARGHERITA', price: 60, id: 'TYPE_MARGHERITA'};
Pizza.TYPE_PEPPERONI = {name: 'PEPPERONI', price: 70, id: 'TYPE_PEPPERONI'};

Pizza.EXTRA_TOMATOES = {name: 'TOMATOES', price: 5, id: 'EXTRA_TOMATOES'};
Pizza.EXTRA_CHEESE = {name: 'CHEESE', price: 7, id: 'EXTRA_CHEESE'};
Pizza.EXTRA_MEAT = {name: 'MEAT', price: 9, id: 'EXTRA_MEAT'};

/* Allowed properties */
Pizza.allowedSizes = ['SIZE_S', 'SIZE_M', 'SIZE_L'];
Pizza.allowedTypes = ['TYPE_VEGGIE', 'TYPE_MARGHERITA', 'TYPE_PEPPERONI'];
Pizza.allowedExtraIngredients = ['EXTRA_TOMATOES', 'EXTRA_CHEESE', 'EXTRA_MEAT'];

// console.log(pizza.getSize());
/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */


/* It should work */ 
// small pizza, type: veggie
let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L.name}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.SIZE_S); // => Invalid ingredient
