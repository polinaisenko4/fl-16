const body = document.querySelector('body');
const lists = document.querySelector('.lists-body');

class UpdateCache {
    constructor() {
        this.cache = new Map();
    }

    storeChange(id, name, value) {
        const existing = this.cache.get(id) || {};
        this.cache.set(id, Object.assign(existing, {
            [name]: value
        }));
    }

    pop(id) {
        const result = this.cache.get(id);
        this.cache.delete(id);
        return result;
    }

    clear() {
        this.cache = new Map();
    }
}

class API {
    constructor(baseURL) {
        this.baseURL = baseURL;
        this.users = [];
    }

    currentUsers() {
        return this.users;
    }

    getUsers() {
        return fetch(`${this.baseURL}/users`, {
            method: 'GET'
        }).then(response => {
            return response.json()
        }).then(json => {
            this.users = json;
            return json;
        });
    }

    updateUser(id, data) {
        return fetch(`${this.baseURL}/user/${id}`, {
            method: 'PUT',
            body: JSON.stringify(data)
        }).then(response => {
            return response.json()
        }).then(json => {
            this.users = this.users.map(user => {
                if (user.id === id) {
                    return Object.assign(user, data);
                }
                return user;
            })
            return json;
        });
    }

    deleteUser(id) {
        return fetch(`${this.baseURL}/user/${id}`, {
            method: 'DELETE'
        }).then(response => {
            return response.json()
        }).then(json => {
            this.users = this.users.filter(user => user.id !== id);
            return json;
        });
    }
}

class Drawer {
    constructor(users, api, cache) {
        this.users = users || [];
        this.api = api;
        this.cache = cache;
    }

    fetchUsers() {
        this.showSpinner()
        return this.api.getUsers().then(users => {
            this.setUsers(users);
        }).catch(console.error).then(() => {
            this.draw();
            this.hideSpinner()
        });
    }

    setUsers(users) {
        this.users = users
    }

    showSpinner() {
        const spinner = document.createElement('div');
        spinner.innerHTML = '<div></div><div></div><div></div><div></div>';
        spinner.classList.add('spinner');

        body.appendChild(spinner);
    }

    hideSpinner() {
        const spinner = document.querySelector('.spinner');
        if (spinner) {
            body.removeChild(spinner);
        }
    }

    draw() {
        lists.innerHTML = '';
        this.users.forEach(user => {

            const userTr = document.createElement('tr');
            userTr.id = user.id;

            const idTd = document.createElement('td');
            idTd.innerText = user.id
            userTr.appendChild(idTd);

            const nameTd = document.createElement('td');
            const nameInput = document.createElement('input');
            nameInput.value = user.name;
            nameInput.addEventListener('change', (ev) => {
                this.cache.storeChange(user.id, 'name', ev.target.value);
            });
            nameTd.appendChild(nameInput);
            userTr.appendChild(nameTd);

            const userNameTd = document.createElement('td');
            userNameTd.innerText = user.username;
            userTr.appendChild(userNameTd);

            const emailTd = document.createElement('td');
            emailTd.innerText = user.email;
            userTr.appendChild(emailTd);

            const addressTd = document.createElement('td');
            addressTd.innerText = `${user.address.city}, ${user.address.street}, ${user.address.suite}`;
            userTr.appendChild(addressTd);

            const phoneTd = document.createElement('td');
            phoneTd.innerText = user.phone;
            userTr.appendChild(phoneTd);

            const websiteTd = document.createElement('td');
            websiteTd.innerText = user.website;
            userTr.appendChild(websiteTd);

            const companyTd = document.createElement('td');
            companyTd.innerText = user.company.name;
            userTr.appendChild(companyTd);

            const updateButtonTd = document.createElement('td');
            const updateButton = document.createElement('button')
            updateButton.innerText = 'EDIT'
            updateButtonTd.appendChild(updateButton);
            userTr.appendChild(updateButtonTd);

            updateButton.addEventListener('click', (ev) => {
                ev.preventDefault();
                this.showSpinner();
                this.api.updateUser(user.id, this.cache.pop(user.id))
                    .catch(console.error)
                    .then(() => {
                        this.hideSpinner();
                        this.setUsers(this.api.currentUsers())
                        this.draw();
                    })
            })

            const deleteButtonTd = document.createElement('td');
            const deleteButton = document.createElement('button')
            deleteButton.innerText = 'DELETE'
            deleteButtonTd.appendChild(deleteButton);
            userTr.appendChild(deleteButtonTd);

            deleteButton.addEventListener('click', (ev) => {
                ev.preventDefault();
                this.showSpinner()
                this.api.deleteUser(user.id)
                    .catch(console.error)
                    .then(() => {
                        this.hideSpinner();
                        this.setUsers(this.api.currentUsers())
                        this.draw();
                    });
            })
            lists.appendChild(userTr);
        })
    }
}

const api = new API('https://jsonplaceholder.typicode.com');
const cache = new UpdateCache();
const drawer = new Drawer([], api, cache);

drawer.fetchUsers()


