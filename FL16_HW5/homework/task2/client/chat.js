const ENTER = 13;
const chatWindow = document.querySelector('.chat-window');
const messageInput = document.querySelector('#message');
const sendButton = document.querySelector('#send');

let username = prompt('What is your name?');

while(!username) {
    username = prompt('Please enter your name');
}

const socket = new WebSocket('ws://localhost:8080');

const onMessageEntered = () => {
    const messageText = messageInput.value;
    if (!messageText) {
        return;
    }
    messageInput.value = ''
    const d = new Date()
    socket.send(JSON.stringify({
        name: username,
        message: messageText,
        date: d.toLocaleString()
    }));
    const message = document.createElement('div');
    message.classList.add('message');
    message.classList.add('message__reverse');

    const userNameDiv = document.createElement('div');
    userNameDiv.innerText = username;
    userNameDiv.classList.add('username')

    const messageTextDiv = document.createElement('div');
    messageTextDiv.innerText = messageText;
    messageTextDiv.classList.add('text')

    const dateDiv = document.createElement('div');
    dateDiv.innerText = d.toLocaleString();
    dateDiv.classList.add('date')

    message.appendChild(userNameDiv);
    message.appendChild(messageTextDiv);
    message.appendChild(dateDiv);
    chatWindow.appendChild(message);

    chatWindow.scrollTop = chatWindow.scrollHeight;
}

socket.addEventListener('open', () => {
    sendButton.addEventListener('click', onMessageEntered);
    messageInput.addEventListener('keyup', (ev) => {
        if (ev.keyCode === ENTER) {
            ev.preventDefault();
            onMessageEntered();
        }
    });
})

socket.addEventListener('message', (event) => {
    console.log(event);
    const message = document.createElement('div');
    const data = JSON.parse(event.data);
    message.classList.add('message');

    const userNameDiv = document.createElement('div');
    userNameDiv.innerText = data.name;
    userNameDiv.classList.add('username')

    const messageTextDiv = document.createElement('div');
    messageTextDiv.innerText = data.message;
    messageTextDiv.classList.add('text')

    const dateDiv = document.createElement('div');
    dateDiv.innerText = data.date;
    dateDiv.classList.add('date')

    message.appendChild(userNameDiv);
    message.appendChild(messageTextDiv);
    message.appendChild(dateDiv);
    chatWindow.appendChild(message);

    chatWindow.scrollTop = chatWindow.scrollHeight;
});