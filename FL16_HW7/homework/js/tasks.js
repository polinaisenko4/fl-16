const NUM_TWO = 2;
const NUM_SIX = 6;
const NUM_NINE = 6;

// Task 1
let arr = ['1', '2', '3', '5', '6', '12', '13', '14', '101', '100'];

function getMaxEvenElement(arr) {
  return arr.filter(el => el % NUM_TWO === 0).reduce((a, b) => Math.max(a, b));
}

//console.log(getMaxEvenElement(arr));

// Task 2

let a = 3;
let b = 5;

[a, b] = [b, a];

// console.log(a);
// console.log(b);

// Task 3

function getValue(a) {
  if (a === undefined || a === null) {
    return '-';
  } else {
    return a
  }
}

// console.log(getValue(1));
// console.log(getValue(0));
// console.log(getValue());
// console.log(getValue(null));

// Task 4

function getObjFromArray(arr) {
  return arr.reduce(function (p, c) {
    p[c[0]] = c[1];
    return p;
  }, {});
}

const arrayOfArrays = [
  ['name', 'Dan'],
  ['age', '21'],
  ['city', 'Lviv'],
  ['job', 'Frontend']
];

//console.log(getObjFromArray(arrayOfArrays))


//Task 5

function addUniqueId(obj) {
  let id = Symbol();
  return { ...obj, id };
}

const obj1 = { name: 'Nick' };

// console.log(addUniqueId(obj1));
// console.log(Object.keys(obj1).includes('id'))

//Task 6

function getRegroupedObject(obj) {
  const {
    name: firstName,
    details: { id, age, univesity }
  } = obj;
  const user = { age, firstName, id };
  return { univesity, user };
}

const oldObj = {
  name: 'willow',
  details: { id: 1, age: 47, univesity: 'LNU' }
};

//console.log(getRegroupedObject(oldObj))

//Task 7

function getArrayWithIUniqueElement(arr) {
  let unique = [...new Set(arr)]
  return unique
}

const arrUnique = ['a', 'b', 'a', 'b'];

//console.log(getArrayWithIUniqueElement(arrUnique))


//Task 8

function hideNumber(str) {
  let s = str.slice(NUM_SIX)
  return s.padStart(NUM_NINE, '*');
}

const phoneNumber = '0123456789';

//console.log(hideNumber(phoneNumber));

//Task 9 

function add(a, b) {
  if (!a && !b) {
    throw new Error('a and b are required');
  }

  if (!b) {
    throw new Error('b is required');
  }
  return a + b
}

//console.log(add(1, 3))

//Task 10

function* generateSequence() {
  yield 'I';
  yield 'LOVE';
  yield 'EPAM';
}

let generator = generateSequence();

for (let value of generator) {
  console.log(value);
}




