const FILTER_SUBSTR = '48';
const ANIMATION_DURATION = 400;

class CalculatorEngine {
  constructor({show, addLog}) {
    this.initialized = false;
    this.tempEntered = false;
    this.value = 0;
    this.tempValue = 0;
    this.show = show;
    this.addLog = addLog;
    this.op = 'none';
  }

  clean() {
    this.initialized = false;
    this.tempEntered = false;
    this.value = 0;
    this.tempValue = 0;
    this.op = 'none';
    this.show(this.value);
  }

  get formattedOp() {
    switch(this.op) {
      case 'plus': {
        return '+'
      }
      case 'minus': {
        return '-'
      }
      case 'multiply': {
        return '*'
      }
      case 'divide': {
        return '/'
      } 
      default: {
        return 'none'
      }
    } 
  }

  setTemp(digit) {
    if (this.initialized && this.op === 'none') {
      return;
    }
    if (this.initialized) {
      this.tempValue = Number(this.tempValue.toString() + digit);
      this.tempEntered = true;
    } else {
      this.value = Number(this.value.toString() + digit);
    }
    this.show(this.initialized ? `${this.value}${this.formattedOp}${this.tempValue}` : this.value);
  }

  changeOp(op) {
    if (this.op !== 'none' && this.tempEntered) {
      this.calc();
    }
    this.op = op;
    this.tempValue = 0;
    this.tempEntered = false;
    this.initialized = true;
    this.show(`${this.value}${this.formattedOp}`);
  }

  calc() {
    if (!this.tempEntered) {
      return;
    }
    const previosValue = this.value;
    switch(this.op) {
      case 'plus': {
        this.value += this.tempValue;
        break;
      }
      case 'minus': {
        this.value -= this.tempValue;
        break;
      }
      case 'multiply': {
        this.value *= this.tempValue;
        break;
      }
      case 'divide': {
        if (this.tempValue === 0) {
          this.clean()
          this.show('Error', true);
          return;
        }
        this.value /= this.tempValue;
        break;
      } 
      default: {
        return;
      }
    }
    this.addLog(`${previosValue}${this.formattedOp}${this.tempValue}=${this.value}`);
    this.tempValue = 0;
    this.tempEntered = false;
    this.op = 'none';
    this.show(this.value);
  }
}

$(document).ready(function () {

  $('#history-content').on('scroll', function() {
    console.log('SCROLL TOP', $('#history-content').scrollTop());
  })

  function show(value, isError) {
    const input = $('#input');
    const hasErrorAlready = input.hasClass('error')
    if (isError && !hasErrorAlready || !isError && hasErrorAlready) {
      input.toggleClass('error')
    }
    input.text(value);
  }

  function addLog(log) {
    const contains48 = log.includes(FILTER_SUBSTR);
    const flex = $('<div/>');
    flex.addClass('history-flex');

    const circle = $('<div/>');
    circle.addClass('history-flex-circle');
    circle.appendTo(flex)

    circle.on('click', function() {
      if (circle.css('background-color') === 'rgb(255, 99, 71)') {
        const styleObject = circle.prop('style'); 
        styleObject.removeProperty('background-color');
      } else {
        circle.css('background-color', '#ff6347');
      }
    })

    const text = $('<div/>', {
      text: log
    });
    if (contains48) {
      text.css('text-decoration', 'underline')
    }
    text.addClass('history-flex-text');
    text.appendTo(flex)

    const cross = $('<div/>', {
      text: '✖'
    });
    cross.addClass('history-flex-cross');
    cross.appendTo(flex)
    
    cross.on('click', function() {
      flex.remove();
    })

    $('#history-content').prepend(flex);
    $('#history-content').animate({
      scrollTop: 0
    }, ANIMATION_DURATION);
  }

  const engine = new CalculatorEngine({show, addLog});

  $('.calc-num').on('click', function() {
    const digit = this.value;
    engine.setTemp(digit);
  });

  $('.calc-oper').on('click', function() {
    const op = this.value;
    engine.changeOp(op);
  })

  $('.calc-equelly').on('click', function() {
    engine.calc();
  })

  $('.calc-сlean').on('click', function() {
    engine.clean();
  })
})

