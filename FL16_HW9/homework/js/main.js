import {
    countUser,
    countComp,
    user,
    comp,
    btn,
    res,
    reset,
    roundField
} from './selectors';

const main = () => {
    let userStep,
        compStep,
        resultUser = 0,
        resultComp = 0,
        blocked = false,
        round = 0;

    const cleanAll = () => {
        resultComp = 0
        comp.innerText = resultComp;
        resultUser = 0
        user.innerText = resultUser;
        btn.forEach(item => item.classList.remove('choice'));
        res.innerText = "Let's play!"
        round = 0;
        roundField.innerText = `${round} Round`;
    }

    const count = () => {
        resultComp = 0
        comp.innerText = resultComp;
        resultUser = 0
        user.innerText = resultUser;
        btn.forEach(item => item.classList.remove('choice'));
        round = 0;
        roundField.innerText = `${round} Round`;
    }


    const choiceComp = () => {
        blocked = true;
        let rand = Math.floor(Math.random() * 3);
        let compFields = countComp.querySelectorAll('.btn');
        compStep = compFields[rand].dataset.field;
        compFields[rand].classList.add('choice');
        winner();
    }

    const choiceUser = (el) => {
        if (blocked) return;
        let target = el.target;
        if (target.classList.contains('btn')) {
            userStep = target.dataset.field;
            btn.forEach(item => item.classList.remove('choice'));
            target.classList.add('choice')
            choiceComp();
        }
    }

    const winner = () => {
        blocked = false;

        let comb = userStep + compStep
        console.log(comb)


        switch (comb) {
            case 'rr':
                round++;
                roundField.innerText = `${round} Round`;
                res.innerText = "Rock vs. Rock, IT'S A DRAW!"
                break

            case 'ss':
                res.innerText = "Scissors vs. Scissors, IT'S A DRAW!"
                round++;
                roundField.innerText = `${round} Round`;
                break

            case 'pp':
                res.innerText = "Paper vs. Paper, IT'S A DRAW!";
                round++;
                roundField.innerText = `${round} Round`;
                break;

            case 'rs':
                res.innerText = "Rock vs. Scissors, You’ve WON!"
                resultUser++;
                round++;
                roundField.innerText = `${round} Round`;
                user.innerText = resultUser;
                break

            case 'sp':
                res.innerText = "Scissors vs. You’ve WON!"
                resultUser++;
                user.innerText = resultUser;
                round++;
                roundField.innerText = `${round} Round`;
                break

            case 'pr':
                res.innerText = "Paper vs. Rock, You’ve WON!";
                resultUser++;
                user.innerText = resultUser;
                round++;
                roundField.innerText = `${round} Round`;
                break;

            case 'sr':
                res.innerText = "Scissors vs. Rock , You’ve LOST!"
                resultComp++;
                comp.innerText = resultComp;
                round++;
                roundField.innerText = `${round} Round`;
                break

            case 'ps':
                res.innerText = "Paper vs. Scissors , You’ve LOST!"
                resultComp++;
                comp.innerText = resultComp;
                round++;
                roundField.innerText = `${round} Round`;
                break

            case 'rp':
                res.innerText = "Rock vs. Paper, You’ve LOST!";
                resultComp++;
                comp.innerText = resultComp;
                round++;
                roundField.innerText = `${round} Round`;
                break;
        }

        if (resultComp === 3) {
            res.innerText = `You’ve LOST! ${resultUser} : ${resultComp}`;

            count()
        }
        if (resultUser === 3) {
            res.innerText = `You’ve WON! Count: ${resultUser} : ${resultComp}`;

            count()
        }
    }

    countUser.addEventListener('click', choiceUser)
    reset.addEventListener('click', cleanAll)
}

export default main;