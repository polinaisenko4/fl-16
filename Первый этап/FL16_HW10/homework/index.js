const isEquals = (a, b) => {
    let result = a === b;
    return result;
}
console.log(isEquals(3, 3));

const isBigger = (a, b) => {
    let result = a >= b;
    return result;
}
console.log(isBigger(5, 1));

const getDifference = (a, b) => {
    if (a > b) {
        return a - b
    } else {
        return b - a
    }
}

console.log(getDifference(5, 10));

const storeNames = (...str) => {
  return str
};

console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));

function negativeCount(numbers) {
    let negativeCount = 0

    numbers.map(number => {
        if (number < 0) {
            negativeCount++
        }
        return 0
    })
    return negativeCount
}

console.log(negativeCount([4, -3, 2, 9]))

const letterCount = (str, letter) => {
    let word = str.split(letter).length - 1
    return word
}

console.log(letterCount('Polina', 'n') )
console.log(letterCount('Konstantin', 't') )

const countPoints = (arr) => {
    let points = 0;
    let i = 0;
    for (i; i < arr.length; i++) {
        let match = arr[i].split(":");
        if (match[0] > match[1]) {
            points += 3;
        } else if (match[0] === match[1]) {
            points += 1;
        } else {
            points += 0;
        }
    }
    return points;
}

console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:40', '99:44', '90:90', '111:100']))
