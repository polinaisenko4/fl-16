function visitLink(path) {
	const currentVisited = localStorage.getItem(path) || '0';
	const currentVisitedInt = parseInt(currentVisited);
	localStorage.setItem(path, currentVisitedInt + 1);
}

function viewResults() {
	const content = document.querySelector('#content');
	const oldUl = document.querySelector('#result');
	if (oldUl) {
		content.removeChild(oldUl)
	}
	const pageOneVisited = localStorage.getItem('Page1') || '0';
	const pageTwoVisited = localStorage.getItem('Page2') || '0';
	const pageThreeVisited = localStorage.getItem('Page3') || '0';
	const ul = document.createElement('ul');
	ul.id = 'result';
	const firstLi = document.createElement('li');
	const secondLi = document.createElement('li');
	const thirdLi = document.createElement('li');
	firstLi.innerText = `You visited Page1 ${pageOneVisited} time(s)`;
	secondLi.innerText = `You visited Page2 ${pageTwoVisited} time(s)`;
	thirdLi.innerText = `You visited Page3 ${pageThreeVisited} time(s)`;
	ul.appendChild(firstLi);
	ul.appendChild(secondLi);
	ul.appendChild(thirdLi);
	content.appendChild(ul);
	localStorage.removeItem('Page1')
	localStorage.removeItem('Page2')
	localStorage.removeItem('Page3')
}