const getAge = (birth) => {
    let now = new Date();
    let year = 0

    if (now.getMonth() < birth.getMonth() || now.getDate() < birth.getDate() && now.getMonth() === birth.getMonth()) {
        year = 1
    }

    let age = now.getFullYear() - birth.getFullYear() - year;

    return age;
}
const BIRTHYEAR = 2000;
const BIRTHMONTH = 9;
const BIRTHDATE = 22;
console.log(getAge(new Date(BIRTHYEAR, BIRTHMONTH, BIRTHDATE)));

let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const getWeekDay = (date) => {

    return days[date.getDay()];
}

console.log(getWeekDay(new Date(BIRTHYEAR, BIRTHMONTH, BIRTHDATE)));

const getAmountDaysToNewYear = (newYear) => {
    const mls = 86400000
    let now = new Date();
    let day = (newYear.getTime() - now.getTime()) / mls;
    return day.toFixed(0)
}

console.log(getAmountDaysToNewYear(new Date('Jan,01,2022,00:00:00')))

const isValidIdentifier = (str) => {
    const regexp = /^[a-zA-Z_$][a-zA-Z_$0-9]*$/;
    const result = regexp.test(str);
    return result
}
console.log(isValidIdentifier('myVar!'))
console.log(isValidIdentifier('myVar$'))
console.log(isValidIdentifier('myVar_1'))
console.log(isValidIdentifier('1_myVar'))

const capitalize = (testStr) => testStr.split(/\s+/).map(word => word[0].toUpperCase() + word.substring(1)).join(' ');
console.log(capitalize('My name is John Smith. I am 27'))

const isValidAudioFile = (str) => {
    const regexp = /(?=.*[A-Za-z])^[^!@#$%^&*()_][A-Za-z]+[.aac|.mp3|.flac|.alac]*$/g;
    const result = regexp.test(str);
    return result
}
console.log(isValidAudioFile('file.mp4'))
console.log(isValidAudioFile('my_file.mp3'))
console.log(isValidAudioFile('file.mp3'))

const getProgrammersDay = (year) => {
    let day = 13;
    const dayLess = 12;
    const month = 8;
    const perOne = 4;
    const perSec = 100;
    const perThird = 400;
    if (year % perOne === 0 && year % perSec !== 0 || year % perThird === 0) {
        day = dayLess;
    }
    let date = new Date(year, month, day)
    return `${day} Sep, ${year} (${getWeekDay(date)})`
}
const progYear = 2020;
console.log(getProgrammersDay(progYear))


const howFarIs = (day) => {
    let date = new Date();
    const num = -1;
    const nowDayIndex = date.getDay();
    const dayIndex = days.findIndex(val => val.toLowerCase() === day.toLowerCase());
    const daysLeft = Math.abs(nowDayIndex - dayIndex);
    if (dayIndex === num) {
        return 'Day name invalid'
    } else if (daysLeft === 0) {
        return `Hey, today is ${getWeekDay(date)} =)`
    } else {
        return `It's ${daysLeft} day(s) left till ${days[dayIndex]}`
    }
}

console.log(howFarIs('thursday'))
console.log(howFarIs('friday'))
console.log(howFarIs('mur'))

const getHexadecimalColors = (text) => {
    const regexp = /#(?:[0-9a-fA-F!]{3}){1,2}\b/gi
    return [...text.matchAll(regexp)].map(val => val[0]);

}

const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';
console.log(getHexadecimalColors(testString));

const isValidPassword = (password) => {
    const regexp = /^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    const result = regexp.test(password);
    return result
}
console.log(isValidPassword('agent007'))
console.log(isValidPassword('AGENT007'))
console.log(isValidPassword('AgentOOO'))
console.log(isValidPassword('Age_007'))
console.log(isValidPassword('Agent007'))

const addThousandsSeparators = (smth) => {
    return smth.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const sep1 = '1234567890';
const sep2 = 1234567890;
console.log(addThousandsSeparators(sep1))
console.log(addThousandsSeparators(sep2))

function getAllUrlsFromText(str) {

    let result = []
    str.replace(/([--:\w?@%&+~#=]*\.[a-z]{2,4}\/{0,2})((?:[?&](?:\w+)=(?:\w+))+|[--:\w?@%&+~#=]+)?/g,
        (el) => result.push(el))
    return result
}
const text1 = 'We use   https://translate.google.com/ to translate some words and phrases from https://angular.io/ ';
const text2 = 'JavaScript is the best language for beginners!';

console.log(getAllUrlsFromText(text1));
console.log(getAllUrlsFromText(text2));