const appRoot = document.getElementById('app-root');

const ARROW_UP = '&#129045;';
const ARROW_DOWN = '&#129047;';
const ARROW_BIDIRECTIONAL = '&#11021;';
const HEADER_AREA_NUMBER = 5;
const HEADER_COUNTRY_NUMBER = 1;

let searchType = '';
let currentSorting = null;
let asc = true;

function handleSort(sorting) {
    return function () {
        console.log(currentSorting);
        if (sorting !== currentSorting && currentSorting) {
            document.querySelector(`#arrow-${currentSorting}`).innerHTML = ARROW_BIDIRECTIONAL;
            asc = null;
        }
        currentSorting = sorting
        if (asc && currentSorting) {
            document.querySelector(`#arrow-${currentSorting}`).innerHTML = ARROW_UP;
        } else {
            document.querySelector(`#arrow-${currentSorting}`).innerHTML = ARROW_DOWN;
        }
        const table = document.querySelector('#countriesTable');
        const tbody = table.querySelector('tbody');
        const rows = tbody.querySelectorAll(`tr`);
        const rowsArr = Array.prototype.slice.call(rows, 0);
        const sorted = rowsArr.sort((v1, v2) => {
            const a = asc
                ? v1.getElementsByTagName('td')[sorting - 1].innerText
                : v2.getElementsByTagName('td')[sorting - 1].innerText
            const b = asc
                ? v2.getElementsByTagName('td')[sorting - 1].innerText
                : v1.getElementsByTagName('td')[sorting - 1].innerText
            return a !== '' && b !== '' && !isNaN(a) && !isNaN(b) ? a - b : a.toString().localeCompare(b)
        });
        asc = !asc;
        sorted.forEach(el => {
            tbody.appendChild(el);
        })
    }
}

function handleSelectChange(event) {
    currentSorting = null;
    asc = null;
    const existingTable = document.querySelector('#countriesTable');
    if (existingTable) {
        appRoot.removeChild(existingTable);
    }
    const existingNoItemsHeader = document.querySelector('#no-items');
    if (existingNoItemsHeader) {
        appRoot.removeChild(existingNoItemsHeader);
    }
    const value = event.target.value;
    const values = searchType === 'language'
        ? externalService.getCountryListByLanguage(value)
        : externalService.getCountryListByRegion(value);
    const table = document.createElement('table');
    table.setAttribute('id', 'countriesTable');
    const thead = document.createElement('thead');
    const headerTr = document.createElement('tr');

    const headerCountryNameTh = document.createElement('th');
    headerCountryNameTh.innerText = 'Country name';
    headerCountryNameTh.addEventListener('click', handleSort(HEADER_COUNTRY_NUMBER));

    const countryArrowSpan = document.createElement('span');
    countryArrowSpan.setAttribute('id', 'arrow-1');
    countryArrowSpan.innerHTML = ARROW_BIDIRECTIONAL;
    headerCountryNameTh.appendChild(countryArrowSpan)

    headerTr.appendChild(headerCountryNameTh);

    const headerCapitalTh = document.createElement('th');
    headerCapitalTh.innerText = 'Capital';
    headerTr.appendChild(headerCapitalTh);

    const headerWorldRegionTh = document.createElement('th');
    headerWorldRegionTh.innerText = 'World Region';
    headerTr.appendChild(headerWorldRegionTh);

    const headerLanguagesTh = document.createElement('th');
    headerLanguagesTh.innerText = 'Languages';
    headerTr.appendChild(headerLanguagesTh);

    const headerAreaTh = document.createElement('th');
    headerAreaTh.innerText = 'Aria';
    headerAreaTh.addEventListener('click', handleSort(HEADER_AREA_NUMBER));
    headerTr.appendChild(headerAreaTh);

    const areaArrowSpan = document.createElement('span');
    areaArrowSpan.setAttribute('id', 'arrow-5');
    areaArrowSpan.innerHTML = ARROW_BIDIRECTIONAL;
    headerAreaTh.appendChild(areaArrowSpan)

    const headerFlagTh = document.createElement('th');
    headerFlagTh.innerText = 'Flag';
    headerTr.appendChild(headerFlagTh);

    const tbody = document.createElement('tbody');

    values.forEach(country => {
        const countryTr = document.createElement('tr');

        const countryNameTd = document.createElement('td');
        countryNameTd.innerText = country.name;
        countryTr.appendChild(countryNameTd);

        const capitalTd = document.createElement('td');
        capitalTd.innerText = country.capital;
        countryTr.appendChild(capitalTd);

        const regionTd = document.createElement('td');
        regionTd.innerText = country.region;
        countryTr.appendChild(regionTd);

        const languagesTd = document.createElement('td');
        languagesTd.innerText = Object.values(country.languages).join(', ');
        countryTr.appendChild(languagesTd);

        const areaTd = document.createElement('td');
        areaTd.innerText = country.area;
        countryTr.appendChild(areaTd);

        const flagTd = document.createElement('td');
        const flagImage = document.createElement('img');
        flagImage.setAttribute('src', country.flagURL);
        flagImage.setAttribute('alt', `${country.name} flag`);
        flagTd.appendChild(flagImage);
        countryTr.appendChild(flagTd);

        tbody.appendChild(countryTr);
    });

    thead.appendChild(headerTr);
    table.appendChild(thead);
    table.appendChild(tbody);
    appRoot.appendChild(table);
}

function handleTypeChange(event) {
    const type = event.target.value;
    searchType = type;
    const existingSelect = document.querySelector('#filterSelect');
    const optionsLabels = type === 'language' ? externalService.getLanguagesList() : externalService.getRegionsList();
    let select;
    const existingTable = document.querySelector('#countriesTable');
    if (existingTable) {
        appRoot.removeChild(existingTable);
    }
    if (!existingSelect) {
        const selectLabel = document.createElement('label');
        selectLabel.setAttribute('for', 'filterSelect');
        selectLabel.innerText = 'Please choose search query:'
        const newSelect = document.createElement('select');
        newSelect.setAttribute('name', 'filterSelect');
        newSelect.setAttribute('id', 'filterSelect');
        newSelect.addEventListener('change', handleSelectChange);
        select = newSelect;
        appRoot.appendChild(selectLabel);
    } else {
        select = existingSelect;
        for (let i = select.length - 1; i >= 0; i--) {
            select.remove(i);
        }
    }
    const headerOption = document.createElement('option');
    headerOption.setAttribute('disabled', true);
    headerOption.setAttribute('selected', true);
    headerOption.innerText = 'Select value';
    select.appendChild(headerOption);
    optionsLabels.forEach(function (option) {
        const optionTag = document.createElement('option');
        optionTag.setAttribute('value', option);
        optionTag.innerText = option;
        select.appendChild(optionTag);
    });
    if (!existingSelect) {
        appRoot.appendChild(select);
    }
    if (!document.querySelector('#no-items')) {
        const noItemsHeader = document.createElement('h1');
        noItemsHeader.setAttribute('id', 'no-items');
        noItemsHeader.innerText = 'No items, please choose search query';
        appRoot.appendChild(noItemsHeader);
    }
};

window.addEventListener('load', function () {
    const header = document.createElement('h1');
    header.setAttribute('id', 'header');
    header.innerText = 'Country search';
    appRoot.appendChild(header)
    const langRadio = document.createElement('input');
    langRadio.type = 'radio';
    langRadio.name = 'searchType';
    langRadio.id = 'langRadio';
    langRadio.value = 'language';
    langRadio.addEventListener('change', handleTypeChange);

    const regionRadio = document.createElement('input');
    regionRadio.type = 'radio';
    regionRadio.name = 'searchType';
    regionRadio.id = 'regionRadio';
    regionRadio.value = 'region';
    regionRadio.addEventListener('change', handleTypeChange);

    const langRadioLabel = document.createElement('label');
    langRadioLabel.classList.add('label');
    langRadioLabel.setAttribute('for', 'langRadio');
    langRadioLabel.innerText = 'Language'

    const regionRadioLabel = document.createElement('label');
    regionRadioLabel.classList.add('label_reg');
    regionRadioLabel.setAttribute('for', 'regionRadio');
    regionRadioLabel.innerText = 'Region'

    const spanTypeOfSearch = document.createElement('span');
    spanTypeOfSearch.innerText = 'Please choose type of search:';

    const radioContainer = document.createElement('div');
    radioContainer.appendChild(langRadio);
    radioContainer.appendChild(langRadioLabel);
    radioContainer.appendChild(regionRadio);
    radioContainer.appendChild(regionRadioLabel);

    appRoot.appendChild(spanTypeOfSearch);
    appRoot.appendChild(radioContainer);
});

