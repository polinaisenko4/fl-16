/* START TASK 1: Your code goes here */

const cells = document.querySelectorAll('td');

const FIRST_COLLUMN = [0, 3, 6];
const SPECIAL_INDEX = 4;

function changeColor() {
    for (let i = 0; i < cells.length; i++) {
        cells[i].onclick = function () {
            if (FIRST_COLLUMN.includes(i)) {
                const indexes = [0, 1, 2].map(item => {
                    return item + 3 * FIRST_COLLUMN.indexOf(i);
                });
                const blueCells = indexes.map(index => {
                    return cells[index];
                });
                blueCells.forEach(c => {
                    if (c.classList.contains('bg-green')) {
                        c.classList.remove('bg-green');
                    }
                    if (!c.classList.contains('bg-yellow')) {
                        c.classList.add('bg-blue');
                    }
                });
            } else if (i === SPECIAL_INDEX) {
                cells.forEach(c => {
                    if (!c.classList.contains('bg-yellow') && !c.classList.contains('bg-blue')) {
                        c.classList.add('bg-green');
                    }
                });
            } else {
                if (cells[i].classList.contains('bg-green')) {
                    cells[i].classList.remove('bg-green');
                }
                if (cells[i].classList.contains('bg-blue')) {
                    cells[i].classList.remove('bg-blue');
                }
                cells[i].classList.add('bg-yellow');
            }
        }
    }
}
changeColor();

/* END TASK 1 */


/* START TASK 2: Your code goes here */
const form = document.getElementById('form');
const number = document.getElementById('number');

form.addEventListener('submit', e => {
    e.preventDefault();

    checkInputs();
});

function checkInputs() {
    const numberValue = number.value.trim();
    if (numberValue === '') {
        setErrorFor(number, 'Number cannot be blank');
    } else if (!isNumber(numberValue)) {
        setErrorFor(number, 'Type number does not follow format +380*********');
    } else {
        setSuccessFor(number, 'Data was successfully sent');
    }
}

function setErrorFor(input, message) {
    const formControl = input.parentElement.parentElement;
    const small = formControl.querySelector('small');
    formControl.className = 'form-control error';
    small.innerText = message;
}

function setSuccessFor(input, message) {
    const formControl = input.parentElement.parentElement;
    const span = formControl.querySelector('small');
    formControl.className = 'form-control success';
    span.innerText = message;
}

function isNumber(number) {
    return /^[+]380[0-9]{9}$/m.test(number);
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */

const BALL_BASE_CENTER = [300 - 20, 165 - 20];
const A_TEAM_BASKET_CENTER = [40, 165];
const B_TEAM_BASKET_CENTER = [600 - 40, 165];

const BALL_RADIUS = 20;
const BASKET_RADIUS = 15;


const task3 = document.getElementById('task3');
const ball = document.getElementById('ball');
const court = document.getElementById('court');

const teamACountField = document.getElementById('team-a-score');
const teamBCountField = document.getElementById('team-b-score');

let teamAScore = 0;
let teamBScore = 0;

let disabled = false;

task3.addEventListener('scoreChanged', (e) => {
    console.log(e);
    const team = e.detail.team;
    if (team === 'A') {
        teamAScore++;
        teamACountField.innerText = teamAScore;
    } else {
        teamBScore++;
        teamBCountField.innerText = teamBScore;
    }

    const notification = document.createElement('div');
    notification.innerText = `Team ${team} score!`;
    notification.style.color = team === 'A' ? 'blue': 'red';
    notification.style.textAlign = 'center';
    task3.appendChild(notification);
    disabled = true;
    setTimeout(() => {
        disabled = false;
        task3.removeChild(notification);
        ball.style.left = `${BALL_BASE_CENTER[0]}px`;
        ball.style.top = `${BALL_BASE_CENTER[1]}px`;
    }, 3000);
}); 

court.addEventListener('click', (e) => {
    if (disabled) {
        return;
    }
    const rect = e.target.getBoundingClientRect();
    const x = e.clientX - rect.left;
    const y = e.clientY - rect.top;
    // console.log(x, y);
    const newBallX = x - 20;
    const newBallY = y - 20;
    const dA = Math.sqrt(Math.pow(newBallX - A_TEAM_BASKET_CENTER[0], 2)
     + Math.pow(newBallY - A_TEAM_BASKET_CENTER[1], 2));
    const dB = Math.sqrt(Math.pow(newBallX - B_TEAM_BASKET_CENTER[0], 2)
     + Math.pow(newBallY - B_TEAM_BASKET_CENTER[1], 2));

    let ev;

    if (checkIfCircleTouches(dA, 15, 20)) {
        ev = new CustomEvent('scoreChanged', {
            detail: {
                team: 'B'
            }
        })
    }
    if (checkIfCircleTouches(dB, 15, 20)) {
        ev = new CustomEvent('scoreChanged', {
            detail: {
                team: 'A'
            }
        })
    }

    if (ev) {
        task3.dispatchEvent(ev)
    }

    ball.style.left = `${newBallX}px`;
    ball.style.top = `${newBallY}px`;
});


const checkIfCircleTouches = (d, r1, r2) => {
    if (d < r1 + r2) {
        return true;
    }
    return false;
};




/* END TASK 3 */