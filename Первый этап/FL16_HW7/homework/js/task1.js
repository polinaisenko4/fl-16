const LOW_AMOUNT = 1000;
const LOW_RATE = 100;
const FORMULA_COEF = 100;
const ROUND_COEF = 2;
let amount = prompt('Inputs initial amount of money');
let amountNum = parseInt(amount);


while (isNaN(amountNum) || amountNum < LOW_AMOUNT) {
    if (isNaN(amountNum)) {
        alert('Values should be numbers');
    }
    if (amountNum < LOW_AMOUNT) {
        alert('Initial amount can’t be less than 1000');
    }
    amount = prompt('Inputs initial amount of money');
    amountNum = parseInt(amount);
}

let years = prompt('Inputs number of years');
let yearsNum = parseInt(years);

while (isNaN(yearsNum) || yearsNum < 1) {
    if (isNaN(yearsNum)) {
        alert('Values should be numbers');
    }
    if (yearsNum < 1) {
        alert('number of years  can’t be less than 1');
    }
    years = prompt('Inputs number of years');
    yearsNum = parseInt(years);
}

let rate = prompt('Inputs percentage of a year.');
let rateNum = parseInt(rate);

while (isNaN(rateNum) || rateNum > LOW_RATE) {
    if (isNaN(rateNum)) {
        alert('Values should be numbers');
    }
    if (rateNum > LOW_RATE) {
        alert('Percentage can’t be bigger than 100. ');
    }
    rate = prompt('Inputs percentage of a year.');
    rateNum = parseInt(rate);
}


const totalAmount = amountNum * Math.pow(1 + rateNum / FORMULA_COEF, yearsNum);
const totalProfit = totalAmount - amountNum;


alert(`Initial amount: ${amountNum}
Number of years: ${yearsNum}
Percentage of year: ${rateNum}

Total profit: ${totalProfit.toFixed(ROUND_COEF)}
Total amount: ${totalAmount.toFixed(ROUND_COEF)}`);

