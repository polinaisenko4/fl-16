// Your code goes here
const LOW_REWARD = 25;
const MIDDLE_REWARD = 50;
const HIGH_REWARD = 100;
const REWARDS = [LOW_REWARD, MIDDLE_REWARD, HIGH_REWARD];
const START_HIGH_BORDER = 8;
const BORDER_STEP = 4;
const INITIAL_ATTEMPTS = 3;

const generateRandomNumber = ({ from, to } = { from: 0, to: 8 }) => {
    const LOW_OFFSET = 0.5;
    const HIGH_OFFSET = 1;
    return Math.round(from - LOW_OFFSET + Math.random() * (to - from + HIGH_OFFSET));
}

const requestInput = (text, formattingRules, validationRules) => {
    let validated = false;
    let result;
    while (!validated) {
        const guessed = prompt(text);
        result = formattingRules && formattingRules.length
            ? formattingRules.reduce((res, rule) => rule(res), guessed)
            : guessed;
        validated = validationRules && validationRules.length
            ? validationRules.every((rule) => rule(result))
            : true;
    }
    return result;
}

let userWantsPlay = confirm('Do you want to play a game');

if (!userWantsPlay) {
    alert('You did not become a billionaire, but can.')
}

let prize = 0;
let attemptsLeft = INITIAL_ATTEMPTS;
let level = 0;

let randomNumber = generateRandomNumber({ from: 0, to: START_HIGH_BORDER + level * BORDER_STEP });

while (userWantsPlay) {
    console.log(randomNumber) // Used for debug
    if (attemptsLeft === 0) {
        alert(`Thank you for your participation. Your prize is: ${prize}$`);
        userWantsPlay = confirm('Do you want play again?');
        if (!userWantsPlay) {
            break;
        }
        attemptsLeft = INITIAL_ATTEMPTS;
        prize = 0;
        level = 0;
        randomNumber = generateRandomNumber({ from: 0, to: START_HIGH_BORDER + level * BORDER_STEP });
    }
    const guessed = requestInput(
        `Choose a toulette pocket number from 0 to ${START_HIGH_BORDER + level * BORDER_STEP}
        Attempts left: ${attemptsLeft}
        Total prize: 0$
        Possible prize on current attempt: ${REWARDS[attemptsLeft - 1] * (level + 1)}`,
        [
            (val) => parseInt(val)
        ],
        [
            (val) => !isNaN(val),
            (val) => val >= 0 && val <= START_HIGH_BORDER + level * BORDER_STEP
        ]
    );
    if (randomNumber === guessed) {
        prize += REWARDS[attemptsLeft - 1] * (level + 1);
        userWantsPlay = confirm(`Congratulation, you won! Your prize is: ${prize}. Do you want to continue?`);
        if (!userWantsPlay) {
            break;
        }
        attemptsLeft = INITIAL_ATTEMPTS;
        level += 1;
        randomNumber = generateRandomNumber({ from: 0, to: START_HIGH_BORDER + level * BORDER_STEP });
    } else {
        attemptsLeft -= 1;
    }
}
