function reverseNumber(num) {
    let string = Math.abs(num).toString();
    let result = string.split('').reverse().join('');
    if (num < 0) {
        result = '-'+result;
    }
    return parseInt(result);
}
console.log(reverseNumber(12345));
console.log(reverseNumber(-56789));

let arr = [2, 5, 8];

function forEach(arr, func) {
    for (let el of arr) {
        func(el);
    }
}
forEach(arr, function (el) {
    console.log(el)
});

function map(arr, func) {
    const result = [];
    for (let el of arr) {
        result.push(func(el));
    }
    return result;
}

console.log(map(arr, function (el) {
    return el + 3
}))

console.log(map([1, 2, 3, 4, 5], function (el) {
    return el * 2
}))

function filter(arr, func) {
    const result = [];
    for (let el of arr) {
        if (func(el)) {
            result.push(el);
        }
    }
    return result;
}

console.log(filter([2, 5, 1, 3, 8, 6], function (el) {
    return el > 3
}))

console.log(filter([1, 4, 6, 7, 8, 10], function (el) {
    return el % 2 === 0
}))

const data = [
    {
        '_id': '5b5e3168c6bf40f2c1235cd6',
        'index': 0,
        'age': 39,
        'eyeColor': 'green',
        'name': 'Stein',
        'favoriteFruit': 'apple'
    },
    {
        'id': '5b5e3168e328c0d72e4f27d8',
        'index': 1,
        'age': 38,
        'eyeColor': 'blue',
        'name': 'Cortez',
        'favoriteFruit': 'strawberry'
    },
    {
        '_id': '5b5e3168cc79132b631c666a',
        'index': 2,
        'age': 2,
        'eyeColor': 'blue',
        'name': 'Suzette',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e31682093adcc6cd0dde5',
        'index': 3,
        'age': 17,
        'eyeColor': 'green',
        'name': 'Weiss',
        'favoriteFruit': 'banana'
    }
]

function getAdultAppleLovers(data) {
    const filtered = filter(data, function (el) {
        return el.age >= 18 && el.favoriteFruit === 'apple';
    })
    const names = map(filtered, function (el) {
        return el.name;
    })
    return names;
}

console.log(getAdultAppleLovers(data));

function getKeys(obj) {
    const result = [];
    for (let key in obj) {
        result.push(key);
    }
    return result
}
function getValues(obj) {
    const result = [];
    for (let key in obj) {
        result.push(obj[key]);
    }
    return result
}

console.log(getKeys({keyOne: 1, keyTwo: 2, keyThree: 3}));
console.log(getValues({keyOne: 1, keyTwo: 2, keyThree: 3}));

const MONTHES = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul','Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

function showFormattedDate(dateObj) {
    const year = dateObj.getFullYear()
    const month = dateObj.getMonth()
    const day = dateObj.getDate()

    return `${day} of ${MONTHES[month]}, ${year}`
}

console.log(showFormattedDate(new Date('2018-08-27T01:10:00')));