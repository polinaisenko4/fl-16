const TIME_REGEX = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;

let eventName = '';

const requestInput = (text, formattingRules, validationRules, def) => {
    let validated = false;
    let result;
    while (!validated) {
        const guessed = prompt(text, def);
        result = formattingRules && formattingRules.length
            ? formattingRules.reduce((res, rule) => rule(res), guessed)
            : guessed;
        validated = validationRules && validationRules.length
            ? validationRules.every((rule) => rule(result))
            : true;
    }
    return result;
}

window.addEventListener('load', () => {
    eventName = requestInput('Enter event name', null, [
        (value) => {
            return !!value;
        }
    ], 'meeting');
});

document.querySelector('#submit').addEventListener('click', (e) => {
    e.preventDefault();
    let name = document.querySelector('#formName').value;
    let time = document.querySelector('#formTime').value;
    let place = document.querySelector('#formPlace').value;
    name = name && name.trim();
    time = time && time.trim();
    place = place && place.trim();
    if (!name || !time || !place) {
        return alert('Input all data');
    }
    if (!TIME_REGEX.test(time)) {
        return alert('Enter time in format hh:mm')
    }
    console.log(`${name} has a ${eventName} today at ${time} somewhere in ${place}`);
});

document.querySelector('#converter').addEventListener('click', (e) => {
    e.preventDefault();
    const euros = requestInput('Enter amount of euros (Please use valid number that greater then 0)', [
        (value) => {
            return parseFloat(value);
        }
    ], [
        (value) => {
            return !!value
        },
        (value) => {
            return !isNaN(value)
        },
        (value) => {
            return value > 0
        }
    ]);
    const dollars = requestInput('Enter amount of dollars (Please use valid number that greater then 0)', [
        (value) => {
            return parseFloat(value);
        }
    ], [
        (value) => {
            return !!value
        },
        (value) => {
            return !isNaN(value)
        },
        (value) => {
            return value > 0
        }
    ]);
    const eurosToHryvnias = euros * 33.52;
    const dollarsToHryvnias = dollars * 27.76;
    alert(`${euros.toFixed(2)} euros are equal ${eurosToHryvnias.toFixed(2)}hrns,
${dollars.toFixed(2)} dollars are equal ${dollarsToHryvnias.toFixed(2)}hrns`)
})